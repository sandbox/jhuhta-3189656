<?php

declare(strict_types = 1);

namespace Drupal\webform_rendered_entity\Plugin\WebformElement;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformMarkupBase;

/**
 * Provides a rendered entity element.
 *
 * @WebformElement(
 *   id = "rendered_entity",
 *   label = @Translation("Rendered Entity"),
 *   description = @Translation("Provides a rendered entity element."),
 *   category = @Translation("Markup elements"),
 *   states_wrapper = TRUE,
 * )
 */
class RenderedEntity extends WebformMarkupBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'entity_type' => '',
      'entity_id' => '',
      'display_mode' => [],
      'wrapper_attributes' => [],
      'attributes' => [],
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['entity'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity settings'),
      '#open' => TRUE,
    ];

    $form['entity']['entity_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity type'),
      '#description' => $this->t('The entity type, such as node or media'),
      '#required' => TRUE,
      '#access' => TRUE,
    ];

    $form['entity']['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity id'),
      '#description' => $this->t('The entity id (node id, media id, ...). You can get it for example from the url when editing the entity.'),
      '#required' => TRUE,
      '#access' => TRUE,
    ];

    $form['entity']['display_mode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity display mode'),
      '#description' => $this->t('The entity display mode (default, liftup_small, large, ...).'),
      '#required' => TRUE,
      '#access' => TRUE,
    ];

    unset($form['markup']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $properties = $this->getConfigurationFormProperties($form, $form_state);

    // Check entity type.
    $entityTypeId = $properties['#entity_type'];
    if (!$this->entityTypeManager->hasDefinition($entityTypeId)) {
      $form_state->setErrorByName('entity_type', $this->t('Entity type %name does not exist.', ['%name' => $entityTypeId]));
      return;
    }

    try {
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('entity_type', $this->t('Unable to load entity type %name.', ['%name' => $entityTypeId]));
      return;
    }

    // Check entity itself.
    $entityId = $properties['#entity_id'];
    if (!is_numeric($entityId)) {
      $form_state->setErrorByName('entity_id', $this->t('Entity id %id needs to be a number.', ['%id' => $entityId]));
      return;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($entityId);
    if (!$entity) {
      $form_state->setErrorByName('entity_id', $this->t('Unable to load id %id of type %type.', ['%id' => $entityId, '%type' => $entityTypeId]));
      return;
    }

    // Make sure it's a content entity.
    if (!$entity instanceof ContentEntityInterface) {
      $form_state->setErrorByName('entity_type', $this->t('Entity type %name is not a content entity.', ['%name' => $entityTypeId]));
      return;
    }

    $displayMode = $properties['#display_mode'];
    $bundle = $entity->bundle();

    $entityViewDisplayId = sprintf('%s.%s.%s', $entityTypeId, $bundle, $displayMode);

    try {
      $entityViewDisplay = $this->entityTypeManager->getStorage('entity_view_display')->load($entityViewDisplayId);
    }
    catch (\Exception $e) {
    }
    if (!$entityViewDisplay) {
      $form_state->setErrorByName('display_mode', $this->t('Unable to load entity view display %entity_view_display.', ['%entity_view_display' => $entityViewDisplayId]));
    }

  }

}
