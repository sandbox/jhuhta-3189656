<?php

namespace Drupal\webform_rendered_entity\Element;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides a responsive image element.
 *
 * @RenderElement("rendered_entity")
 */
class RenderedEntity extends RenderElement implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'webform_rendered_entity',
      '#input' => FALSE,
      '#pre_render' => [
        [$class, 'preRenderRenderedEntity'],
      ],
    ];
  }

  /**
   * Prepares a rendered render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderRenderedEntity(array $element) {
    $element['#name'] = 'webform-rendered-entity';
    $element['#attributes']['class'][] = 'webform-rendered-entity';

    $entityTypeId = $element['#entity_type'];
    $entityId = $element['#entity_id'];
    $displayMode = $element['#display_mode'];
    $entityTypeManager = \Drupal::entityTypeManager();

    // Entity type existence.
    if (!$entityTypeManager->hasDefinition($entityTypeId)) {
      return $element;
    }

    try {
      $entityStorage = $entityTypeManager->getStorage($entityTypeId);
    }
    catch (\Exception $e) {
      // Unable to load entity type.
      return $element;
    }

    // Check entity itself.
    if (!is_numeric($entityId)) {
      // What did we have as an eid?
      return $element;
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($entityId);
    // Unable to load or not a content entity.
    if (!$entity || !$entity instanceof ContentEntityInterface) {
      return $element;
    }

    $viewBuilder = $entityTypeManager->getViewBuilder($entityTypeId);
    $element['content'] = $viewBuilder->view($entity, $displayMode);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'preRenderRenderedEntity',
    ];
  }

}
