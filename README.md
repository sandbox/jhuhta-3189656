# Webform Element Rendered Entity

A WebformElement plugin module for providing a feature to embed rendered entities
as Webform elements.

## Introduction

This module is an easy solution for you to be able to attach i.e. images or other
media on a Webform.

## Requirements

As this module enhances [Webform][1] module for [Drupal 8+][2], those are the
obvious requirements. There's no other requirements.

## Installing

Install as you would [normally install][3] a contributed Drupal module.

## Configuration

Enable the module and you'll be able to create elements of type "Rendered Entity".
Element configuration requires you to type in the entity type (such as node or media),
entity id and display mode (such as "medium").

The entity, if available, will be rendered on the form using those parameters.

## Credits

Original module was developed by [Jukka Huhta][4] of [Siili Solutions][5] in an
[Aalto University][6] project.


[1]: https://www.drupal.org/project/webform
[2]: https://www.drupal.org/docs/8
[3]: https://www.drupal.org/documentation/install/modules-themes/modules-8
[4]: https://www.drupal.org/u/jhuhta
[5]: https://www.siili.com/
[6]: https://www.aalto.fi/
